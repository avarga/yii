<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $full_name
 * @property string $email
 * @property integer $admin
 *
 * The followings are the available model relations:
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, full_name, email', 'required'),
			array('admin', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>45),
			array('password', 'length', 'max'=>100),
			array('full_name, email', 'length', 'max'=>200),
			// @todo Please remove those attributes that should not be searched.
			array('id, username, full_name, email, admin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'azonosító',
			'username' => 'felhasználónév',
			'password' => 'jelszó',
			'full_name' => 'teljes név',
			'email' => 'e-mail',
			'admin' => 'oldal tartalmának szerkesztésére jogosult',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('admin',$this->admin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param null $id
     * @param bool $useSession
     * @return User
     */
    public static function getUser($id=null, $useSession = false){
        if ($id) return User::model()->findByPk($id);

        if ($useSession){
            $session = new CHttpSession;
            $session->open();
        }
        if ($useSession && isset($session['user'])&& $session['user']) return $session['user'];

        $user = User::model()->findByPk(Yii::app()->user->getId());

        if ($useSession)
            $session['user'] = $user;

        return $user ? $user : new User();
    }

    /**
     * @return User[]
     */
    public static function getAdminUsers(){
        return User::model()->findAll('approval = 1');
    }

    /** get all the admins e-mail
     * @return string
     */
    public static function getAdminUsersMailAddress(){
        $addresses = array();
        foreach (User::getAdminUsers() as $user){
            $addresses[]= $user->email;
        }

        return implode(',', $addresses);
    }


    /**
     * @return boolean
     */
    public function isAdmin(){
        return $this->admin;
    }


	function __toString()
	{
		return $this->username;
	}


}
