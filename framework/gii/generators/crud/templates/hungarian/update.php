<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->class2name($this->entityName);
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Szerkesztés',
);\n";
?>

$this->menu=array(
	array('label'=>'<?php echo $this->entityName; ?> lista', 'url'=>array('index')),
	array('label'=>'<?php echo $this->entityName; ?> létrehozása', 'url'=>array('create')),
	array('label'=>'<?php echo $this->entityName; ?> megtekintése', 'url'=>array('view', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'<?php echo $this->entityName; ?> kezelés', 'url'=>array('admin')),
);
?>

<h1><?php echo $this->entityName; ?> szerkesztése</h1>

<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>