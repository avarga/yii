<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->class2name($this->entityName);
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Létrehozás',
);\n";
?>

$this->menu=array(
	array('label'=>'<?php echo $this->entityName; ?> lista', 'url'=>array('index')),
	array('label'=>'<?php echo $this->entityName; ?> kezelés', 'url'=>array('admin')),
);
?>

<h1><?php echo $this->entityName; ?> létrehozás</h1>

<?php echo "<?php \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
