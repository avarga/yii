<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */

/** @var CrudCode $this */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php
echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->class2name($this->entityName);
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>'<?php echo $this->entityName; ?> lista', 'url'=>array('index')),
	array('label'=>'<?php echo $this->entityName; ?> létrehozása', 'url'=>array('create')),
	array('label'=>'<?php echo $this->entityName; ?> szerkesztése', 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'<?php echo $this->entityName; ?> törlése', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Biztosan törölni akarod??')),
	array('label'=>'<?php echo $this->entityName; ?> kezelés', 'url'=>array('admin')),
);
?>

<h1><?php echo $this->entityName; ?></h1>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>
